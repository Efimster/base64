fn main() {
    let result = base64::decode_bytes("FYvBCYBADARb2QrsxJ/gW46FBKKRJIfavXcwrxlmpzU/iXKs3Up7qS3YRBODklnc8AiD+LyjHRfGcQczpwgU34LHDw==".as_bytes());
    println!("decode result: {:02x?}", &result);
}