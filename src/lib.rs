use std::str;
pub mod url_safe_util;

const BITS_DIFF: usize = 8 - 6;
const PADDING_B4: u8 = 0x4d;


pub fn encode(bytes: &[u8]) -> String {
    let bytes: &[u8] = &encode_bytes(bytes);
    str::from_utf8(bytes).unwrap().to_string()
}

pub fn decode(bytes: &[u8]) -> String {
    let bytes:&[u8] = &decode_bytes(bytes);
    str::from_utf8(bytes).unwrap().to_string()
}

pub fn encode_bytes(bytes: &[u8]) -> Box<[u8]> {
    let result_length = encoded_length_for(bytes);

    let mut result = Vec::with_capacity(result_length);
    for index in 0..result_length {
        let start_bit = index * 6;
        let start_byte = start_bit >> 3;

        let start_bit = (start_bit % 8) as u8;
        let b64 = if start_byte >= bytes.len() {
            PADDING_B4
        } else if start_bit <= BITS_DIFF as u8 {
            bytes[start_byte] >> (BITS_DIFF as u8 - start_bit) & 0b0011_1111
        } else {
            let mut result = (bytes[start_byte] & (0xff >> start_bit)) << start_bit - BITS_DIFF as u8;
            if start_byte + 1 < bytes.len() {
                result |= bytes[start_byte + 1] >> 10 - start_bit;
            }
            result
        };

        //println!("b64 {} , {} , ascii code {}", b64, b'a', byte_to_ascii(b64));
        result.push(b64_to_ascii(b64));
    }

    result.into_boxed_slice()
}

pub fn decode_bytes(bytes: &[u8]) -> Box<[u8]> {
    let potential_length = (bytes.len() * 6) >> 3;
    let mut start_bit: u8 = 0;
    let mut byte: u8 = 0;
    let mut result: Vec<u8> = Vec::with_capacity(potential_length);

    for index in 0..bytes.len() {
        let ascii = bytes[index];

        if ascii == b'=' {
            break;
        }

        let b64 = ascii_to_b64(ascii);

        if start_bit <= BITS_DIFF as u8 {
            byte |= b64 << (BITS_DIFF as u8 - start_bit);
            if start_bit == BITS_DIFF as u8 {
                result.push(byte);
                byte = 0;
            }
        } else {
            byte |= b64 >> start_bit - BITS_DIFF as u8;
            result.push(byte);
            byte = b64 << (8 - start_bit + BITS_DIFF as u8);
        }
        start_bit = (start_bit + 6) % 8;
    }

    result.into_boxed_slice()
}

fn b64_to_ascii(b64: u8) -> u8 {
    if b64 < 26 {
        b'A' + b64
    } else if b64 < 52 {
        b'a' + b64 - 26
    } else if b64 < 62 {
        b'0' + b64 - 52
    } else if b64 == 62 {
        b'+'
    } else if b64 == 63 {
        b'/'
    } else {
        b'='
    }
}

fn ascii_to_b64(ascii: u8) -> u8 {
    if ascii >= b'a' {
        26 + ascii - b'a'
    } else if ascii >= b'A' {
        ascii - b'A'
    } else if ascii >= b'0' {
        52 + ascii - b'0'
    } else if ascii == b'+' {
        62
    } else if ascii == b'/' {
        63
    } else {
        0xff
    }
}

pub fn encoded_length_for(bytes: &[u8]) -> usize {
    let bits_length = bytes.len() << 3;

    let mut result_length = bits_length / 6;
    if bits_length % 6 > 0 {
        result_length += (8 - (bits_length % 6)) / BITS_DIFF;
    }
    result_length
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_result_length() {
        assert_eq!(encoded_length_for(b"a"), 4);
        assert_eq!(encoded_length_for(b"ab"), 4);
        assert_eq!(encoded_length_for(b"abc"), 4);
        assert_eq!(encoded_length_for(b"abcd"), 8);
        assert_eq!(encoded_length_for(b"abcde"), 8);
        assert_eq!(encoded_length_for(b"abcdef"), 8);
    }

    #[test]
    fn test_encode() {
        let base64 = encode("Ma".as_bytes());
        assert_eq!(base64, "TWE=");

        let base64 = encode("Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.".as_bytes());
        assert_eq!(base64, "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=");
    }

    #[test]
    fn test_b64_to_ascii() {
        assert_eq!(b64_to_ascii(0), b'A');
        assert_eq!(b64_to_ascii(9), b'J');
        assert_eq!(b64_to_ascii(40), b'o');
        assert_eq!(b64_to_ascii(51), b'z');
        assert_eq!(b64_to_ascii(52), b'0');
        assert_eq!(b64_to_ascii(61), b'9');
        assert_eq!(b64_to_ascii(62), b'+');
        assert_eq!(b64_to_ascii(63), b'/');
        assert_eq!(b64_to_ascii(PADDING_B4), b'=');
    }

    #[test]
    fn test_ascii_to_b64() {
        assert_eq!(ascii_to_b64(b'A'), 0);
        assert_eq!(ascii_to_b64(b'J'), 9);
        assert_eq!(ascii_to_b64(b'o'), 40);
        assert_eq!(ascii_to_b64(b'z'), 51);
        assert_eq!(ascii_to_b64(b'0'), 52);
        assert_eq!(ascii_to_b64(b'9'), 61);
        assert_eq!(ascii_to_b64(b'+'), 62);
        assert_eq!(ascii_to_b64(b'/'), 63);
        //assert_eq!(ascii_to_b64(b'='), PADDING_B4);
    }

    #[test]
    fn test_decode() {
        let message = decode("TWE=".as_bytes());
        assert_eq!(message, "Ma");

        let message = decode("TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=".as_bytes());
        assert_eq!(message, "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.");
    }
}